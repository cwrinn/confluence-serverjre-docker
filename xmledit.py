#!/usr/bin/python

# Imports
import argparse
import logging
import os
from lxml import etree

def _copy_items(items):
    '''Based on the _copy_items from argparse.py'''
    if items is None:
        i = []
    elif type(items) is list:
        i = items[:]
    else:
        import copy
        i = copy.copy(items)
    return i

class XMLAction(argparse.Action):
    def __init__(self, option_strings, dest, nargs='+', **kwargs):
        if nargs is not '+':
            raise ValueError("nargs must be +")
        super(XMLAction, self).__init__(option_strings, dest, nargs=nargs, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        d = {
                'xpath': values.pop(0),
                'action': self.dest,
                'values': {},
            }

        for value in values:
            k,v = value.split('=')
            d['values'][k] = v

        items = getattr(namespace, 'xml_actions', None)
        items = _copy_items(items)
        items.append(d)
        setattr(namespace, 'xml_actions', items)


class XMLEditor(object):
    tree = None
    path = None
    output = None
    actions = []
    clean = False
    strip_whitespace = False
    strip_comments = False
    readable = False

    def __init__(self,
                 path,
                 output=None,
                 actions=[],
                 clean=False,
                 strip_whitespace=False,
                 strip_comments=False,
                 readable=False,
                ):
        self.path = path
        self.output = output
        self.actions = actions
        self.clean = clean
        self.strip_whitespace = strip_whitespace
        self.strip_commends = strip_comments
        self.readable = readable

        self.open()


    def open(self):
        remove_blank_text = self.clean or self.strip_whitespace
        remove_comments = self.clean or self.strip_comments

        parser = etree.XMLParser(remove_blank_text=remove_blank_text, remove_comments=remove_comments)
        self.tree = etree.ElementTree(file=self.path, parser=parser)

    def perform_actions(self):
        if self.actions:
            for action in self.actions:
                getattr(self, action['action'])(action)


    def write(self):
        pretty_print = self.clean or self.readable
        output = self.output or self.path
        if output:
            #self.tree.write(output)
            with open(output, 'w') as f: #Because ElementTree.write has seemingly NO way to "pretty print"
                f.write(etree.tostring(self.tree,pretty_print=pretty_print))


    def update(self, data={}):
        xpath = data.get('xpath', None)
        values = data.get('values',{})
        text = values.pop('text', None)
        for e in self.tree.xpath(xpath):
            if text:
                e.text = text
            e.attrib.update(data['values'])


# Functions
def parse_args(args=None):
  parser = argparse.ArgumentParser()
  parser.add_argument('--update', '-u', action=XMLAction)
  parser.add_argument('--verbose', '-v', dest='verbosity', action='count', default=0)
  parser.add_argument('--strip-comments', '-s', action='store_true')
  parser.add_argument('--strip-whitespace', '-w', action='store_true')
  parser.add_argument('--readable', '-r', action='store_true')
  parser.add_argument('--clean', '-c', action='store_true')
  parser.add_argument('--file', '-f')
  parser.add_argument('--output', '-o')
  return parser.parse_args(args)


def setup_logging(verbosity):
    log_level = max(logging.ERROR - (verbosity * 10), logging.DEBUG)
    logging.basicConfig(level=log_level)


# Main
def main():
    args = parse_args()
    setup_logging(args.verbosity)
    xml = XMLEditor(path=args.file,
                    output=args.output,
                    actions=args.xml_actions,
                    clean=args.clean,
                    strip_whitespace=args.strip_whitespace,
                    strip_comments=args.strip_comments,
                    readable=args.readable,
                   )
    xml.perform_actions()
    xml.write()


if __name__ == '__main__':
  main()

