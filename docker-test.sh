#!/bin/bash
set -e
(/docker-entrypoint.sh)&
sleep 5m
curl http://localhost:8090/
